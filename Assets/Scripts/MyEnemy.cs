﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class MyEnemy : MonoBehaviour {

    public int Health = 100;
    public int Damage;
    public float Speed;
    Vector3 Dir = new Vector3(0, 0, 0);

	public void Start()
	{
        Dir.x = -Speed;
	}

	public void FixedUpdate()
	{
        transform.position += Dir;
	}

    public void Hurt(int Damage)
    {
        Health -= Damage;
        if (Health <= 0)
        {
            Destroy(gameObject);
        }
	}
}
