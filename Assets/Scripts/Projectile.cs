﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Projectile : MonoBehaviour {

	public GameObject BOOM; // Объект, который спаунится после взрыва
	public int Damage; // Урон, который мы нанесём
	public float Speed, LifeTime; // Скорость и время жизни снаряда
	Vector3 Dir = new Vector3 (0,0,0); // Направление полёта
	void Start ()
	{
		Dir.x = Speed; // Говорим, что всегда летим по оси х
		Destroy(gameObject, LifeTime); // Говорим, что этот объект уничтожится через установленное время
	}
	void FixedUpdate ()
    {
		transform.position += Dir; // Движение снаряда
	}
	void OnTriggerEnter2D(Collider2D collision)
	{
        print(collision.gameObject.tag);
		if (collision.gameObject.tag == "Enemy") // Если объект с которым мы столкнулись имеет тэг Enemy
		{
			collision.GetComponent<MyEnemy>().Hurt(Damage); // Вызываем метод урона и говорим сколькоурона
			Instantiate(BOOM, transform.position, transform.rotation); // Спауним объект, который симулирует взрыв
            Destroy(gameObject); // Уничтожаем пулю
		}
		
	}
}
