﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Boom : MonoBehaviour {

    public float LifeTime = 1;

	void Start () {
        Destroy(gameObject, LifeTime);
	}
}
