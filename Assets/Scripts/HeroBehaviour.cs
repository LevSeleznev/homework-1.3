﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class HeroBehaviour : MonoBehaviour {

	public float Acceleration; // Скорость движения, а в дальнейшем ускорение
	public GameObject Bullet, StartBullet; // Наш снаряд которым будем стрелять и точка, где он создаётся
	Vector3 Dir = new Vector3 (0 , 0 ,0); // Направление движения
	Vector3 WheelRotate = new Vector3 (0, 0, 0); // Поворот колёс
	void FixedUpdate()
	{
		if (Input.GetKey(KeyCode.RightArrow) || Input.GetKey(KeyCode.D)) // Если нажата стрелка вправо или D
		{
			Dir.x = Acceleration; // Направление движения идёт по оси x в положительную сторону
			WheelRotate.z = -Acceleration; // Поворот колеса по оси z в отрицательную сторону
		}
		else if (Input.GetKey(KeyCode.LeftArrow) || Input.GetKey(KeyCode.A)) // Иначе, если нажата стрелка влевоили A
		{
			Dir.x = -Acceleration; // Направление движения идёт по оси x в отрицательную сторону Else Если у нас ничего не нажато
			WheelRotate.z = Acceleration; // Поворот колеса по оси z в положительную сторону
		}
		else
		{
			Dir.x = 0; // Стоим на месте
			WheelRotate.z = 0;
		}
		transform.position += Dir * Time.deltaTime; // Передаём нашему transform движение
		transform.GetChild (1).gameObject.transform.Rotate (WheelRotate); // Крутим заднее колесо
		transform.GetChild (2).gameObject.transform.Rotate (WheelRotate); // Крутим переднее колесо
		if (Input.GetButtonDown("Fire1")) // Если нажата кнопка выстрела
		{
            Instantiate(Bullet, StartBullet.transform.position, transform.rotation); // Создаём Bullet в точке StartBullet
		}
	}
}
